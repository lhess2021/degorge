;;; degorge.el --- Track calorie intake in Org Mode -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2023 Lorenzo Hess
;;
;; Author: Lorenzo Hess <lorenzohess@tutanota.com>
;; Maintainer: Lorenzo Hess <lorenzohess@tutanota.com>
;; Created: October 19, 2023
;; Modified: October 19, 2023
;; Version: 0.0.1
;; Keywords: outlines
;; Homepage: https://github.com/lorenzohess/degOrge
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;; Make sure to add this capture templates to `org-capture-templates'. That's
;; how you will enter each meal into the calories datetree. Use this snippet
;; as the template:
;;   ("d" "Degorge" entry
;;   (file+olp+datetree "/path/to/degOrge/Meals.org")
;;   (file "/path/to/degOrge/MealCaptureTemplate.org"))
;;
;; 1. DONE Ask user for food
;; 2. DONE Search for food heading in Foods.org
;; 3. DONE Copy food item
;; 4. DONE Paste food item into meal buffer
;; 5. DONE Get QUANTITY_CACHE, UNITS_CACHE, CAL_PER_UNIT properties of meal food
;; 6. DONE Ask user for quantity with QUANTITY_CACHE list for completion
;; 7. DONE Ask user for unit with UNITS_CACHE list for completion
;; 7. DONE Get CAL_PER_<UNIT> property
;; 8. DONE Calculate calories for this meal
;; 9. DONE Add CALORIES property to the meal
;; 10. DONE Replace :food: tag with :meal: tag
;; 11. DONE Cut buffer contents
;;
;;  Description
;;
;;; Code:

(require 'cl-lib)
(require 'org)
(require 'org-element)

(defvar degorge--dir "/home/user/nextcloud-sync.back/projects/degOrge")
(defvar degorge--meal-file (concat degorge--dir "/Meals.org"))
(defvar degorge--foods-file (concat degorge--dir "/Foods.org"))
(defvar degorge--temp-meal-file "degorge-meal")

(defun degorge--mapfunc ()
  "The function to run on each food entry."
  (org-entry-get nil "ITEM"))

(defun degorge--make-foods-list ()
  "Make a list of foods from the headings in degorge--foods-database."
  ;; (interactive)
  (org-map-entries #'degorge--mapfunc))

(defun degorge--choose-food (foods)
  "Choose a food from FOODS."
  ;; (cdr (assoc (completing-read "Food: " foods) foods)))
  (completing-read "Food: " foods))

(defun degorge--goto-food-heading (food)
  "Find position of FOOD heading in `degorge--foods-file'."
  (goto-char (point-min))
  (search-forward (concat "* " food)))

(defun degorge--copy-food-item ()
  "Copy the tree at point."
  (org-copy-subtree))

(defun degorge--create-new-meal-buffer ()
  "Generate a blank meal buffer in which to construct and edit meals."
  (let ((meal-buffer (format "%s" (get-buffer-create degorge--temp-meal-file))))
    (with-current-buffer meal-buffer (progn
                                       (erase-buffer)
                                       (org-mode)))))

(defun degorge--paste-food-into-meal-buffer ()
  "Insert the food item in the kill ring into the meal buffer."
  (with-current-buffer degorge--temp-meal-file (org-paste-subtree)))

(defun degorge--get-heading-at-point-property (prop)
  "Use `org-entry-get' to get the value of property PROP for the heading at point."
  (org-entry-get (point) prop))

(defun degorge--ask-for-quantity (quantity-cache)
  "Ask the user for a quantity using QUANTITY-CACHE as completion candidates."
  (completing-read "Quantity: " quantity-cache))

(defun degorge--ask-for-quantity-unit (quantity-units-cache)
  "Ask the user for a quantity using QUANTITY-UNITS-CACHE as completion candidates."
  (completing-read "Quantity units: " quantity-units-cache))

(defun degorge--get-cal-per-unit-list (food-props unit)
  "Return value of CAL_PER_<UNIT> property in FOOD-PROPS.

  Example:
  :PROPERTIES:
  :CAL_PER_oz: 10
  :CAL_PER_cup: 80
  :END:"
  (cdr (assoc
        (format "%s" (concat "CAL_PER_" (upcase unit)))
        food-props)))

(defun degorge--add-meal-quantity (quantity)
  "Add QUANTITY as value of MEAL_QUANTITY property to meal at point.")

(defun degorge-run ()
  "Return a meal Org entry."
  (interactive)
  (with-current-buffer degorge--foods-file
    ;; Make food list
    (setq foods-list (degorge--make-foods-list)))
  ;; Ask user for food
  (sleep-for 1)
  (setq food (degorge--choose-food foods-list))
  (with-current-buffer degorge--foods-file
    ;; Go to food heading
    (degorge--goto-food-heading food)
    ;; Copy food heading
    (degorge--copy-food-item))
  ;; Make meal buffer
  (degorge--create-new-meal-buffer)
  ;; Paste food into meal buffer
  (degorge--paste-food-into-meal-buffer)
  ;; Get properties
  (with-current-buffer degorge--temp-meal-file (setq food-properties (org-entry-properties)))
  ;; Wait for org-reveal timer to complete to avoid race condition error
  (sleep-for 0.1)
  ;; Ask user for quantity and units
  (with-current-buffer degorge--temp-meal-file (setq quantity (degorge--ask-for-quantity (org-entry-get-multivalued-property (point) "QUANTITY_CACHE"))))
  (with-current-buffer degorge--temp-meal-file (setq unit (degorge--ask-for-quantity-unit (org-entry-get-multivalued-property (point) "QUANTITY_UNITS_CACHE"))))
  ;; Find calories per unit
  (setq cal-per-unit (degorge--get-cal-per-unit-list food-properties unit))
  ;; Calculate meal calories
  (setq cals (* (string-to-number quantity) (string-to-number cal-per-unit)))
  ;; Add meal calories property
  (with-current-buffer degorge--temp-meal-file (org-entry-put (point) "MEAL_CALORIES" (number-to-string cals)))
  ;; Change food tag to meal tag
  (with-current-buffer degorge--temp-meal-file
    (progn (org-toggle-tag "food")
           (org-toggle-tag "meal")))
  ;; Copy buffer contents
  (setq meal (with-current-buffer degorge--temp-meal-file (buffer-substring (point-min) (point-max)))))

(provide 'degorge)
;;; degorge.el ends here
